package com.multimod;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileView {
    public void showFile(String fileName) {
        // Prints the contents of the text file to console.
        // Implement it using commons-io.
        System.out.println( "Reading pom file:" );
        File file = new File(fileName);
        List<String> lines = null;
        try {
            lines = FileUtils.readLines(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String line : lines) {
            System.out.println(line);
        }
    }
}
